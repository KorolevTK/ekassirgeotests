package MatchingTypeGeo;


import org.testng.annotations.*;

import java.net.HttpURLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ParamCityMatchingTypeString {
	 private HttpURLConnection connection;
		private JsonArray items;

	    @BeforeSuite
	    public  void setSuite() {
	        try {
	            connection = null;
	            final String uri="https://mtest.ekassir.com:4443/personalcabinet/api/v1/geo/poi";
	            CloseableHttpClient client = HttpClients.createDefault();
	            CloseableHttpResponse response = client.execute(new HttpGet(uri));
	            HttpEntity entity = response.getEntity();
	            String data = IOUtils.toString(entity.getContent(), "UTF-8");
	            JsonParser parser = new JsonParser();
	            Object obj =parser.parse(data);
	            JsonObject ja = (JsonObject)obj;
	            items = (JsonArray)ja.get("items");
	        } catch (Throwable cause) {
	            cause.printStackTrace();
	        }
	    }
	   
	    @AfterSuite
	    public void setOutSuite()  {
			if (connection != null) {
	            connection.disconnect();
	        }
	    }
	   
	    @Test
	    public void paramCityMatchingTypeString () throws Throwable {
	                for (JsonElement o: items) {
	                    o.getAsJsonObject().get("city").getAsString();
//	                	throw new ClassCastException();
	                }
	    }
}
